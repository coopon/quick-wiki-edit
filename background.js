var WIKIPEDIA_SITES = ["*://*.wikipedia.org/*"]

function promptSelection(text){
  return "window.prompt('Change your text'" + ",'" + text + "');"
}

function executeScript(selectionText) {
  executing = browser.tabs.executeScript({
    code: promptSelection(selectionText)
  });

  executing.then(onExecuted, onError)
}

function onExecuted(result){
  console.log(result[0]);
}

function onError(error) {
  console.log(error);
}

function onCreated() {
  if (browser.runtime.lastError)
    console.log(browser.runtime.lastError);
  else
    console.log("Item created Sucessfully");
}

browser.contextMenus.create({
  id: "quick-edit",
  documentUrlPatterns: WIKIPEDIA_SITES,
  title: "Quick Edit",
  contexts: ["selection"]
}, onCreated);

browser.contextMenus.onClicked.addListener(function(info, tab) {
  switch (info.menuItemId) {
    case "quick-edit":
      executeScript(info.selectionText);
      break;
  }
});
