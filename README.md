### QuickWikiEdit

A Firefox Extension to quickly edit small words in Wikipedia without using the Wikipedia's Editor.

----

#### Try Out

The extension is not yet ready and not uploaded to AMO (Mozilla's Addon Store). To try out this extension, do the following.

1. Clone this repo.
2. Open Firefox -> `about:debugging` -> `Load Temporary Addon`
3. Select any of the file from the cloned directory and the Extension should be loaded.
4. Open any wikipedia website -> Select a word or two -> right click -> Quick Edit

Note: The context menu `Quick Edit` appears only on Wikipedia websites.
